package pl.edu.pjwstk.gdansk.s10908.nai;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;

import java.net.URL;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class NaiController implements Initializable {

    @FXML
    private ToggleButton captureToggler;

    @FXML
    private Button restartButton;

    @FXML
    private Button savePatternButton;

    @FXML
    private Button loadPatternsButton;

    @FXML
    private ImageView originalImage;

    @FXML
    private ImageView maskImage;

    @FXML
    private ImageView maskTrackImage;

    @FXML
    private ColorPicker minColor;

    @FXML
    private ColorPicker maxColor;


    private VideoCapture capture;

    private ScheduledExecutorService timer;

    private Scalar minHSV;
    private Scalar maxHSV;

    private Mat paintedPattern;
    private List<Mat> probes;

    private static final Supplier<Mat> MAT = Mat::new;
    private static final String PROBES_DIR = "probes";

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        capture = new VideoCapture();

        minColor.setValue(Color.hsb(Config.HSV_MIN_INIT[0], Config.HSV_MIN_INIT[1], Config.HSV_MIN_INIT[2]));
        maxColor.setValue(Color.hsb(Config.HSV_MAX_INIT[0], Config.HSV_MAX_INIT[1], Config.HSV_MAX_INIT[2]));
        minHSV = Utils.color2Scalar(minColor.getValue());
        maxHSV = Utils.color2Scalar(maxColor.getValue());

        paintedPattern = MAT.get();
        probes = Utils.reloadProbes(PROBES_DIR, probes);

        minColor.setOnAction((event) -> minHSV = Utils.color2Scalar(minColor.getValue()));
        maxColor.setOnAction((event) -> minHSV = Utils.color2Scalar(maxColor.getValue()));

        captureToggler.selectedProperty().addListener((observableValue, wasSelected, isSelected) -> toggleCapture(isSelected));
        restartButton.setOnAction(e -> paintedPattern = MAT.get());
        savePatternButton.setOnAction(e -> Utils.saveProbe(paintedPattern.clone(), String.format("%s/%s.png", PROBES_DIR, UUID.randomUUID().toString().substring(0, 8))));

        loadPatternsButton.setOnAction(e -> probes = Utils.reloadProbes(PROBES_DIR, probes));
    }

    public void toggleCapture(boolean selected) {
        if (selected) {
            start();
        } else {
            stop(originalImage, maskImage, maskTrackImage);
        }
    }

    private void start() {
        if (!capture.open(Config.WEBCAM_DEVICE)) {
            throw new RuntimeException("Could not open the camera connection");
        }
        timer = Executors.newSingleThreadScheduledExecutor();
        timer.scheduleAtFixedRate(this::processFrame, 0, 25, TimeUnit.MILLISECONDS);
    }

    private void processFrame() {
        final Mat frame = Utils.grabFrame(this.capture);
        Core.flip(frame, frame, Config.MIRROR_FLIP);
        Utils.fxProperty(originalImage.imageProperty(), Utils.mat2Image(frame));

        Mat blurredImage = MAT.get();
        Mat hsvImage = MAT.get();
        Imgproc.blur(frame, blurredImage, Config.BLUR_SIZE);
        Imgproc.cvtColor(blurredImage, hsvImage, Imgproc.COLOR_BGR2HSV);

        Mat mask = MAT.get();
        Core.inRange(hsvImage, minHSV, maxHSV, mask);
        Utils.erode(mask, mask, Config.ERODE_ELEMENT, Config.ERODE_TIMES);
        Utils.dilate(mask, mask, Config.DILATE_ELEMENT, Config.DILATE_TIMES);

        Mat isolatedObject = MAT.get();
        Core.bitwise_and(frame, frame, isolatedObject, mask);
        Utils.fxProperty(maskImage.imageProperty(), Utils.mat2Image(isolatedObject));

        Core.bitwise_or(mask, paintedPattern.total() == 0 ? mask : paintedPattern, paintedPattern);
        Utils.fxProperty(maskTrackImage.imageProperty(), Utils.mat2Image(paintedPattern));
    }

    private void stop(ImageView... imgs) {
        try {
            timer.shutdown();
            timer.awaitTermination(115, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            System.err.println("Exception in stopping frame capture.");
            e.printStackTrace();
        } catch (NullPointerException e) {
            System.err.println("Timer wasn't initialized");
            e.printStackTrace();
        } finally {
            capture.release();
            Arrays.stream(imgs).forEach(img -> Utils.fxProperty(img.imageProperty(), null));
        }
    }
}
