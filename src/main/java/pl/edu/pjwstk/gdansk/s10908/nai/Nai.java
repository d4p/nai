package pl.edu.pjwstk.gdansk.s10908.nai;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.opencv.core.Core;

public class Nai extends Application {

    private static final EventHandler<WindowEvent> EXIT = e -> {
        Platform.exit();
        System.exit(0);
    };

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("nai.fxml"));
        primaryStage.setTitle("NAI project");
        primaryStage.setScene(new Scene(root));
        primaryStage.setResizable(false);
        primaryStage.show();
        primaryStage.setOnCloseRequest(EXIT);
    }

    public static void main(String[] args) {
        launch(args);
    }

}
