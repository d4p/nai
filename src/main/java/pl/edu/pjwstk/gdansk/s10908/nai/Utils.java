package pl.edu.pjwstk.gdansk.s10908.nai;

import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.highgui.VideoCapture;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by damian on 28.01.16.
 */
public class Utils {

    public static <T> void fxProperty(final ObjectProperty<T> property, final T value) {
        Platform.runLater(() -> property.set(value));
    }

    public static Mat grabFrame(VideoCapture capture) {
        Mat frame = null;
        if (capture.isOpened()) {
            frame = new Mat();
            try {
                capture.read(frame);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return frame;
    }


    public static Image mat2Image(Mat frame) {
        int cols = frame.cols();
        int rows = frame.rows();
        int elemSize = (int) frame.elemSize();
        int channels = frame.channels();
        byte[] data = new byte[cols * rows * elemSize];
        int type;
        frame.get(0, 0, data);

        if (channels == 1) {
            type = BufferedImage.TYPE_BYTE_GRAY;
        } else if (channels == 3) {
            type = BufferedImage.TYPE_3BYTE_BGR;
            bgr2rgb(data);
        } else {
            return null;
        }
        BufferedImage image = new BufferedImage(cols, rows, type);
        image.getRaster().setDataElements(0, 0, cols, rows, data);

        return SwingFXUtils.toFXImage(image, null);

    }

    public static Mat image2Mat(BufferedImage image) {
        byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        Mat mat = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3);
        mat.put(0, 0, data);
        return mat;
    }

    public static void bgr2rgb(byte[] data) {
        byte b;
        for (int i = 0; i < data.length; i += 3) {
            b = data[i];
            data[i] = data[i + 2];
            data[i + 2] = b;
        }
    }

    public static void erode(Mat src, Mat dst, Mat element, int times) {
        do { Imgproc.erode(src, dst, element); } while(--times > 0);
    }

    public static void dilate(Mat src, Mat dst, Mat element, int times) {
        do { Imgproc.dilate(src, dst, element); } while(--times > 0);
    }

    public static Scalar color2Scalar(Color color) {
        return new Scalar(color.getHue()/2, color.getSaturation() * 255, color.getBrightness() * 255);
    }

    public static void saveProbe(Mat probe, String fileName) {
        BufferedImage bImage = SwingFXUtils.fromFXImage(Utils.mat2Image(probe), null);
        try {
            ImageIO.write(bImage, "png", new File(fileName));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public static List<Mat> reloadProbes(String probesDir, List<Mat> probes) {
        if(probes == null) {
            probes = new ArrayList<>();
        } else {
            probes.clear();
        }
        try {
            for(File file : new File(probesDir).listFiles()) {
                probes.add(Utils.image2Mat(ImageIO.read(file)));
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return probes;
    }
}
