package pl.edu.pjwstk.gdansk.s10908.nai;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

/**
 * Created by damian on 30.01.16.
 */
public class Config {
    public static final int WEBCAM_DEVICE = 0;
    public static final int MIRROR_FLIP = 1;
    public static final Size BLUR_SIZE = new Size(4, 4);
    public static final int DILATE_TIMES = 1;
    public static final Size DILATE_ELEMENT_SIZE = new Size(24, 24);
    public static final int ERODE_TIMES = 1;
    public static final Size ERODE_ELEMENT_SIZE = new Size(12, 12);
    public static final Mat DILATE_ELEMENT = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, DILATE_ELEMENT_SIZE);
    public static final Mat ERODE_ELEMENT = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, ERODE_ELEMENT_SIZE);
    public static final double[] HSV_MIN_INIT = {110*2, 50.0/255, 50.0/255};
    public static final double[] HSV_MAX_INIT = {130*2, 1.0, 1.0};
}
